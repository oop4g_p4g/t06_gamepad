#include <windows.h>
#include <string>
#include <cassert>
#include <d3d11.h>
#include <vector>

#include "WindowUtils.h"

#include "Game.h"

using namespace std;
using namespace DirectX;
using namespace DirectX::SimpleMath;

#include <Xinput.h>

//gamepad data, one per pad
struct State
{
	int port = -1;
	float leftStickX = 0;
	float leftStickY = 0;
	XINPUT_STATE state;
};
State gPads[XUSER_MAX_COUNT];
const int STICK_RANGE = 32767;
//check if pads are plugged in and what the left stick is doing
void UpdatePads()
{
	for (DWORD i = 0; i < XUSER_MAX_COUNT; i++)
	{
		State& s = gPads[i];
		s.port = -1;
		ZeroMemory(&s.state, sizeof(XINPUT_STATE));
		if (XInputGetState(i, &s.state) == ERROR_SUCCESS)
		{
			float normLX = fmaxf(-1, (float)s.state.Gamepad.sThumbLX / STICK_RANGE);
			float normLY = fmaxf(-1, (float)s.state.Gamepad.sThumbLY / STICK_RANGE);

			if(normLX!=0)
				s.leftStickX = abs(normLX) * (normLX / abs(normLX));
			if(normLY!=0)
				s.leftStickY = abs(normLY) * (normLY / abs(normLY));

			s.port = i;
		}
	}
}

/*
Is the user holding down a button or stick
XINPUT_GAMEPAD_DPAD_UP
XINPUT_GAMEPAD_DPAD_DOWN
XINPUT_GAMEPAD_DPAD_LEFT
XINPUT_GAMEPAD_DPAD_RIGHT
XINPUT_GAMEPAD_START
XINPUT_GAMEPAD_BACK
XINPUT_GAMEPAD_LEFT_THUMB
XINPUT_GAMEPAD_RIGHT_THUMB
XINPUT_GAMEPAD_LEFT_SHOULDER
XINPUT_GAMEPAD_RIGHT_SHOULDER
XINPUT_GAMEPAD_A
XINPUT_GAMEPAD_B
XINPUT_GAMEPAD_X
XINPUT_GAMEPAD_Y
*/
bool IsPadPressed(int idx, unsigned short buttonId)
{
	assert(idx >= 0 && idx < XUSER_MAX_COUNT);
	assert(gPads[idx].port != -1);
	return (gPads[idx].state.Gamepad.wButtons & buttonId) != 0;
}

//is this one plugged in?
bool IsConnected(int idx) {
	assert(idx >= 0 && idx < XUSER_MAX_COUNT);
	return gPads[idx].port != -1;
}

//if ALT+ENTER or resize or drag window we might want do
//something like pause the game perhaps, but we definitely
//need to let D3D know what's happened (OnResize_Default).
void OnResize(int screenWidth, int screenHeight, MyD3D& d3d)
{
	d3d.OnResize_Default(screenWidth, screenHeight);
}


//messages come from windows all the time, should we respond to any specific ones?
LRESULT CALLBACK MainWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	//do something game specific here
	switch (msg)
	{
		// Respond to a keyboard event.
	case WM_CHAR:
		switch (wParam)
		{
		case 27:
		case 'q':
		case 'Q':
			PostQuitMessage(0);
			break;
		}
	}

	//default message handling (resize window, full screen, etc)
	return WinUtil::DefaultMssgHandler(hwnd, msg, wParam, lParam);
}


//main entry point for the game
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance,
				   PSTR cmdLine, int showCmd)
{

	int w(512), h(256);
	//int defaults[] = { 640,480, 800,600, 1024,768, 1280,1024 };
		//WinUtil::ChooseRes(w, h, defaults, 4);
	if (!WinUtil::Get().InitMainWindow(w, h, hInstance, "Fezzy", MainWndProc, true))
		assert(false);

	MyD3D d3d;
	if (!d3d.InitDirect3D(OnResize))
		assert(false);
	WinUtil::Get().SetD3D(d3d);
	d3d.GetCache().SetAssetPath("data/");
	Game game(d3d);
	
	bool canUpdateRender;
	float dTime = 0;
	while (WinUtil::Get().BeginLoop(canUpdateRender))
	{
		if (canUpdateRender && dTime>0)
		{
			game.Update(dTime);
			game.Render(dTime);
		}
		dTime = WinUtil::Get().EndLoop(canUpdateRender);
		UpdatePads();
	}

	game.Release();
	d3d.ReleaseD3D(true);	
	return 0;
}

